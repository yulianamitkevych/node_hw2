const Note = require('../models/note');

const handleClientError = (res, error) => {
  console.log(error);
  res.status(400).send({message: 'Client error'});
};

const handleServerError = (res, error) => {
  console.log(error);
  res.status(500).send({message: 'Server error'});
};

const createNote = (req, res) => {
  try {
    const {text} = req.body;
    const {id} = req.user;
    const note = new Note({text, userId: id});
    note
        .save()
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => {
          console.log(error);
          handleClientError(res, error);
        });
  } catch (error) {
    handleServerError(res, error);
  }
};

const getNote = (req, res) => {
  try {
    const id = req.params.id;
    Note
        .findById(id)
        .then((note) => res.status(200).send({note: note}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

const getNotes = (req, res) => {
  try {
    const {offset, limit} = req.query;
    const perPage = Math.max(0, +limit);
    Note
        .find()
        .skip(+offset || 0)
        .limit(+limit || 0)
        .then((notes) =>
          res.status(200).send({
            offset: +offset || 0, limit: +limit || 0, count: Math.floor(perPage / +offset || 0), notes: notes
          }))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

const deleteNote = (req, res) => {
  try {
    const id = req.params.id;
    Note
        .findByIdAndDelete(id)
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

const updateNote = (req, res) => {
  try {
    Note
        .findByIdAndUpdate(req.params.id, req.body)
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

const checkNote = (req, res) => {
  try {
    Note
        .findById(req.params.id)
        .then((note) => note
            .updateOne(note.completed === false ? {completed: true} : {completed: false})
            .then(() => res.status(200).send({message: 'Success'}))
            .catch((error) => handleClientError(res, error)))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

module.exports = {
  createNote,
  getNote,
  getNotes,
  deleteNote,
  updateNote,
  checkNote
};
