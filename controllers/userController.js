const User = require('../models/user');
const bcrypt = require('bcryptjs');

const handleClientError = (res, error) => {
  console.log(error);
  res.status(400).send({message: 'Client error'});
};

const handleServerError = (res, error) => {
  console.log(error);
  res.status(500).send({message: 'Server error'});
};

const getUserInfo = async (req, res) => {
  try {
    const user = await User.findOne({username: req.user.username});
    const {_id, username, createdDate} = user;
    return res.status(200).send({
      user: {
        _id,
        username,
        createdDate
      }
    });
  } catch (error) {
    console.log(error);
    handleServerError(res, error);
  }
};

const deleteUser = async (req, res) => {
  try {
    User.findByIdAndDelete(req.user.id)
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    console.log(error);
    handleServerError(res, error);
  }
};

const changePassword = async (req, res) => {
  try {
    const hashPassword = bcrypt.hashSync(req.body.newPassword, 7);
    User.findByIdAndUpdate(req.user.id, {password: hashPassword})
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    console.log(error);
    handleServerError(res, error);
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changePassword
};
