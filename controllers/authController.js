const User = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');

const generateAccessToken = (id, username) => {
  const payload = {
    id,
    username
  };
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

const handleClientError = (res, error) => {
  console.log(error);
  res.status(400).send({message: 'Client error'});
};

const handleServerError = (res, error) => {
  console.log(error);
  res.status(500).send({message: 'Server error'});
};

const registerUser = (req, res) => {
  try {
    const {username, password} = req.body;
    const hashPassword = bcrypt.hashSync(password, 7);
    const user = new User({username, password: hashPassword});
    user
        .save()
        .then(() => res.status(200).send({message: 'Success'}))
        .catch((error) => handleClientError(res, error));
  } catch (error) {
    handleServerError(res, error);
  }
};

const loginUser = async (req, res) => {
  try {
    const {username, password} = req.body;
    const user = await User.findOne({username});
    if (!user) {
      return res.status(400).send({message: 'Client error'});
    }
    const validPassword = bcrypt.compare(password, user.password);
    if (!validPassword) {
      return res.status(400).send({message: 'Client error'});
    }
    const token = generateAccessToken(user._id, user.username);
    return res.status(200).send({message: 'Success', jwt_token: token});
  } catch (error) {
    handleServerError(res, error);
  }
};

module.exports = {
  registerUser,
  loginUser
};
