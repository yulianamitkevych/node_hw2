const express = require('express');
const {getUserInfo, deleteUser, changePassword} = require('../controllers/userController');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware');

router.get('/api/users/me', authMiddleware, getUserInfo);
router.delete('/api/users/me', authMiddleware, deleteUser);
router.patch('/api/users/me', authMiddleware, changePassword);

module.exports = router;
