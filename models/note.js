const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = new Schema({
  userId: {
    type: String,
    trim: true
  },
  text: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  createdDate: {
    type: Date,
    default: new Date().toISOString()
  }
}, {versionKey: false});

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
