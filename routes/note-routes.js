const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware');
const {createNote, getNote, getNotes, deleteNote, updateNote, checkNote} = require('../controllers/noteController');

router.post('/api/notes', authMiddleware, createNote);
router.get('/api/notes/:id', authMiddleware, getNote);
router.get('/api/notes', authMiddleware, getNotes);
router.delete('/api/notes/:id', authMiddleware, deleteNote);
router.put('/api/notes/:id', authMiddleware, updateNote);
router.patch('/api/notes/:id', authMiddleware, checkNote);

module.exports = router;
